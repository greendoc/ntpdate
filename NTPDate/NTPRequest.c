//
//  NTPRequest.c
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/12/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#include <stdio.h>

#include "NTPRequest.h"

int mode = 3;
long currenttimestamp;
struct ntpDatagram data;


void create_request () {
    memset(&data, 0, sizeof(struct ntpDatagram));
    data.flags = NTP_VERSION;
}

int send_request (int socket_descriptor, struct sockaddr * address, socklen_t len) {
    if (sendto(socket_descriptor, &data, sizeof(struct ntpDatagram), 0, (struct sockaddr *) address, len) == -1) {
        perror("No se pudo enviar el datagrama");
        return 1;
    }
    return 0;
}

