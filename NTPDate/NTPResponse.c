//
//  NTPResponse.c
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/12/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#include <stdio.h>
#include "NTPResponse.h"

struct ntpDatagram data;

unsigned int recv_secs;

time_t total_secs;
struct tm *now;

long read_response (int socket_descriptor, struct sockaddr *address, socklen_t len) {
    recvfrom(socket_descriptor, &data, sizeof(struct ntpDatagram), 0, address, &len);
    return handle_response(data);
}

long handle_response (struct ntpDatagram packet) {
    /* correct for right endianess */
    packet.root_delay = ENDIAN_SWAP32(packet.root_delay);
    packet.root_dispersion = ENDIAN_SWAP32(packet.root_dispersion);
    packet.ref_ts_sec = ENDIAN_SWAP32(packet.ref_ts_sec);
    packet.ref_ts_frac = ENDIAN_SWAP32(packet.ref_ts_frac);
    packet.origin_ts_sec = ENDIAN_SWAP32(packet.origin_ts_sec);
    packet.origin_ts_frac = ENDIAN_SWAP32(packet.origin_ts_frac);
    packet.recv_ts_sec = ENDIAN_SWAP32(packet.recv_ts_sec);
    packet.recv_ts_frac = ENDIAN_SWAP32(packet.recv_ts_frac);
    packet.trans_ts_sec = ENDIAN_SWAP32(packet.trans_ts_sec);
    packet.trans_ts_frac = ENDIAN_SWAP32(packet.trans_ts_frac);
    
    /* print raw data */
    printf("LI: %u\n", LI_BITMASK(packet.flags));
    printf("VN: %u\n", VN_BITMASK(packet.flags));
    printf("Mode: %u\n", MODE_BITMASK(packet.flags));
    printf("stratum: %u\n", packet.stratum);
    printf("poll: %u\n", packet.poll);
    printf("precision: %u\n", packet.precision);
    printf("root delay: %u\n", packet.root_delay);
    printf("root dispersion: %u\n", packet.root_dispersion);
    printf("reference ID: %u.", packet.referenceID[0]);
    printf("%u.", packet.referenceID[1]);
    printf("%u.", packet.referenceID[2]);
    printf("%u\n", packet.referenceID[3]);
    printf("reference timestamp: %u.", packet.ref_ts_sec);
    printf("%u\n", packet.ref_ts_frac);
    printf("origin timestamp: %u.", packet.origin_ts_sec);
    printf("%u\n", packet.origin_ts_frac);
    printf("receive timestamp: %u.", packet.recv_ts_sec);
    printf("%u\n", packet.recv_ts_frac);
    printf("transmit timestamp: %u.", packet.trans_ts_sec);
    printf("%u\n", packet.trans_ts_frac);
    
    /* print date with receive timestamp */
    recv_secs = packet.recv_ts_sec - UNIX_OFFSET; /* convert to unix time */
    total_secs = recv_secs;
    printf("Unix time: %u\n", (unsigned int)total_secs);
    now = localtime(&total_secs);
    printf("%02d/%02d/%d %02d:%02d:%02d\n", now->tm_mday, now->tm_mon+1, \
           now->tm_year+1900, now->tm_hour, now->tm_min, now->tm_sec);
    
    return total_secs;
}