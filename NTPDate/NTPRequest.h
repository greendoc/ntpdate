//
//  NTPRequest.h
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/12/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#ifndef SNTP_DATE_NTPRequest_h
#define SNTP_DATE_NTPRequest_h

#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "Datagram.h"

#define NTP_VERSION 		0xe3

/** functions definitions */
void create_request ();
int send_request (int socket_descriptor, struct sockaddr * address, socklen_t len);

#endif
