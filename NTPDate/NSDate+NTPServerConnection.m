//
//  NSDate+NTPServerConnection.m
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/14/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#import "NSDate+NTPServerConnection.h"
#import "ServerConnector.h"

@implementation NSDate (NTPServerConnection)

- (void) syncDate {
    long l = init_connection();
    l += (60 * 60) * 2;
    self = [NSDate dateWithTimeIntervalSince1970:l];
}

@end
