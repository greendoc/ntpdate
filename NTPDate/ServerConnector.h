//
//  ServerConnector.h
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/11/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#ifndef SNTP_DATE_ServerConnector_h
#define SNTP_DATE_ServerConnector_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>

#endif

long init_connection ();