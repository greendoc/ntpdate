//
//  NTPResponse.h
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/12/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#ifndef SNTP_DATE_NTPResponse_h
#define SNTP_DATE_NTPResponse_h

#define UNIX_OFFSET 2208988800L
#define VN_BITMASK(byte) ((byte & 0x3f) >> 3)
#define LI_BITMASK(byte) (byte >> 6)
#define MODE_BITMASK(byte) (byte & 0x7)
#define ENDIAN_SWAP32(data) ((data >> 24) | /* right shift 3 bytes */ \
((data & 0x00ff0000) >> 8) | /* right shift 1 byte */ \
((data & 0x0000ff00) << 8) | /* left shift 1 byte */ \
((data & 0x000000ff) << 24)) /* left shift 3 bytes */

#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "Datagram.h"

long read_response (int socket_descriptor, struct sockaddr *address, socklen_t len);
long handle_response (struct ntpDatagram packet);

#endif
