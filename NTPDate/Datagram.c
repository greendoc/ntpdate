

#include <stdio.h>

#include "Datagram.h"

long to_ntp_timestamp (long ts) {
    long ntpTs = ts / 1000 + DIFF_EPOCHS;
    ntpTs <<= 32;
    return ntpTs;
}


long from_ntp_timestamp (long ntpts) {
    long seconds = ntpts >> 32;
    long fraction = ntpts & 0xFFFFFFFFL;
    long ts = (seconds - DIFF_EPOCHS) * 1000;
    ts += fraction * 1000 / 4294967296L;
    return ts;
}
