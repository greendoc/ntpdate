/**
 * SNTP - Simple Network Protocol, V4, RFC-2030
 *
 *
 *
 * The SNTP protocol is described in RFC-2030. The NTP time server is listening on UDP port 123.
 * SNTP formats are compatible to the NTP protocol specification, using a simplified access strategy
 * for servers and clients; the access paradigm is identical to the UDP/TIME protocol.
 *
 * While the NTP protocol is a sophisticated time synchronization protocol, the DAYTIME, TIME and
 * SNTP protocol are very easy to handle.
 */

#include "ServerConnector.h"
#include "NTPRequest.h"
#include "NTPResponse.h"

#define PORT "123"
#define URL_SERVER "time.euro.apple.com"


int ds;
socklen_t addrlen = sizeof(struct sockaddr_storage);
struct addrinfo hints, *res, *ap;

//struct sockaddr_in server;
struct sockaddr server;

long init_connection () {
    //Create socket
    if ((ds = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("Error creating socket");
        exit(-1);
    }
    
    
    
    //Init server variable
    if (getaddrinfo(URL_SERVER, PORT, &hints, &ap) != 0) {
        perror("no");
        exit(-1);
    }
    ds = socket(ap->ai_family, ap->ai_socktype, ap->ai_protocol);
    
    create_request();
    send_request(ds, ap->ai_addr, sizeof(server));

    printf("enviada solicitud");
    
    return read_response(ds, ap->ai_addr, sizeof(server));
}