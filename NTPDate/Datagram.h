//
//  Datagram.h
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/12/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#ifndef SNTP_DATE_Datagram_h

#include <stdint.h>

#define SNTP_DATE_Datagram_h

#define DIFF_EPOCHS 2208988800L

long from_ntp_timestamp (long ntpts);
long to_ntp_timestamp (long ts);


/** 
 *  Definition of NTP datagram. 
 *  Package sent for each comunication between server and client will 
 *  have this architecture.
 */
struct ntpDatagram {
    uint8_t flags;
    uint8_t stratum;
    uint8_t poll;
    uint8_t precision;
    uint32_t root_delay;
    uint32_t root_dispersion;
    uint8_t referenceID[4];
    uint32_t ref_ts_sec;
    uint32_t ref_ts_frac;
    uint32_t origin_ts_sec;
    uint32_t origin_ts_frac;
    uint32_t recv_ts_sec;
    uint32_t recv_ts_frac;
    uint32_t trans_ts_sec;
    uint32_t trans_ts_frac;
};

#endif
