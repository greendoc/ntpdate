//
//  NSDate+NTPServerConnection.h
//  SNTP_DATE
//
//  Created by Angel Fernandez Alvarez on 6/14/12.
//  Copyright (c) 2012 EQUALTER. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (NTPServerConnection)

- (void) syncDate;

@end
